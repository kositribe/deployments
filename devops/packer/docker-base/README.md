# Docker Base

To build locally:
 1. Build `kositribe/controller:latest`
    - `cd devops/docker/dockerfiles/controller`
    - `docker build -t kositribe/controller:latest .`

 2. Run packer build within `kositribe/controller:latest`
    - `cd devops/packer/docker-base`
    - then:
      ```
      docker run \
          --rm \
          --env-file=/path/to/env/file/containing/AWS_or_HCLOUD/creds \
          -v ${PWD}:${PWD} \
          --workdir=${PWD} \
          kositribe/controller:latest \
          packer build [-only=amazon-ebs|hcloud] template.json
      ```
